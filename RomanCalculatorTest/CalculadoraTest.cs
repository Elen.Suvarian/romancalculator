using NUnit.Framework;
using RomanCalculator;
using System;

namespace RomanCalculatorTest
{
    public class Tests
    {
        private Calculadora calculadora;
        [SetUp]
        public void Setup()
        {
            calculadora = new Calculadora();
        }

        [Test]
        public void SumaImasI()
        {
            Assert.That(calculadora.Suma("I", "I"), Is.EqualTo("II"));
        }

        [Test]
        public void SumaValoresVacios()
        {
            Assert.That(calculadora.Suma("", ""), Is.EqualTo(""));
        }

        [Test]
        public void RestaValorMenorIzquierda()
        {
            Assert.That(calculadora.Suma("IX", "IX"), Is.EqualTo("XVIII"));
        }


        [Test]
        public void SumaValoresConcatenados()
        {
            Assert.That(calculadora.Suma("XX", "II"), Is.EqualTo("XXII"));
        }


        [Test]
        public void ValoresMayorA4000()
        {
            Assert.Throws<ArgumentException>(() => calculadora.Suma("MMM", "MMM"));
        }

        [Test]
        public void ConcatenacionTripleIXC()
        {
            Assert.Throws<ArgumentException>(() => calculadora.Suma("IIII", "CCCC"));
        }

        [Test]
        public void CompruebaUnicaUnidadVLD()
        {
            Assert.Throws<ArgumentException>(() => calculadora.Suma("DD", "LLL"));
        }
    }
}