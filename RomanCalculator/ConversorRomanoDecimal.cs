﻿using System;

namespace RomanCalculator
{
    class ConversorRomanoDecimal
    {
        Constantes constantes;

        public int ConvertirRomanoADecimal(string numeroRomano)
        {

            CompruebaTripleIXC(numeroRomano);
            CompruebaUnicaUnidadVLD(numeroRomano);

            constantes = new Constantes();
            //retornamos un 0 en caso de que no se reciba un numero
            if (numeroRomano.Length == 0) return 0;

            int numeroConvertido = 0;
            int ultimoValor = 0;
            for (int i = numeroRomano.Length - 1; i >= 0; i--)
            {
                int valorNuevo = constantes.numerosRomanos[numeroRomano[i]];

                // Restamos si el valor nuevo es menor que el ultimo valor transformado a decimal
                if (valorNuevo < ultimoValor)
                    numeroConvertido -= valorNuevo;
                else
                { //Sumamos si el ultimo valor es mayor que el ultimo valor transformado
                    numeroConvertido += valorNuevo;
                    ultimoValor = valorNuevo;
                }
            }

            return numeroConvertido;
        }


        private bool CompruebaTripleIXC(string numeroRomano)
        {
            if (numeroRomano.Contains("IIII") || numeroRomano.Contains("XXXX") || numeroRomano.Contains("CCCC"))
            {
                throw new ArgumentException("Formato de número romano incorrecto");
            }

            return true;
        }

        private bool CompruebaUnicaUnidadVLD(string numeroRomano)
        {
            if (numeroRomano.Contains("VV") || numeroRomano.Contains("LL") || numeroRomano.Contains("DD"))
            {
                throw new ArgumentException("Formato de número romano incorrecto");
            }

            return true;
        }
    }
}
