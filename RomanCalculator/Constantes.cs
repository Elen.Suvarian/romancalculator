﻿using System.Collections.Generic;

namespace RomanCalculator
{
    class Constantes
    {

        public Dictionary<char, int> numerosRomanos = new Dictionary<char, int>{
                {'I', 1},
                {'V', 5},
                { 'X', 10},
                { 'L', 50},
                { 'C', 100},
                { 'D', 500},
                { 'M', 1000}
            };

        public string[] Unitarios = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
        public string[] Dieces = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
        public string[] Cienes = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
        public string[] Miles = { "", "M", "MM", "MMM" };
    }
}
