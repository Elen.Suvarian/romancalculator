﻿using System;

namespace RomanCalculator
{
    public class Calculadora
    {        

        private ConversorRomanoDecimal conversorRomanoDecimal;
        private ConversorDecimalRomano conversorDecimalRomano;

        public String Suma(string primerNumero, string segundoNumero)
        {
            conversorRomanoDecimal = new ConversorRomanoDecimal();
            conversorDecimalRomano = new ConversorDecimalRomano();

            try
            {
                int primeroSumando = conversorRomanoDecimal.ConvertirRomanoADecimal(primerNumero);
                int segundoSumando = conversorRomanoDecimal.ConvertirRomanoADecimal(segundoNumero);

                return conversorDecimalRomano.ConvertirDecimalARomano(primeroSumando + segundoSumando);
            }
            catch(Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
            
        }

    }
}
