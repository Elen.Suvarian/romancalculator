﻿using System;

namespace RomanCalculator
{
    class ConversorDecimalRomano
    {
        Constantes constantes;

        public string ConvertirDecimalARomano(int numeroDecimal)
        {
            constantes = new Constantes();

            string numeroConvertido = "";
            int numeroAuxiliar;

            // Si es mayor o igual que 4000 devolvemos una excepción
            if (numeroDecimal >= 4000)
            {
                throw new ArgumentException("Número mayor o igual que 4000");
            }

            //Buscamos los Miles
            numeroAuxiliar = numeroDecimal / 1000;
            numeroConvertido += constantes.Miles[numeroAuxiliar];
            numeroDecimal %= 1000;

            //Buscamos los Cientos
            numeroAuxiliar = numeroDecimal / 100;
            numeroConvertido += constantes.Cienes[numeroAuxiliar];
            numeroDecimal %= 100;

            //Buscamos los Dieces
            numeroAuxiliar = numeroDecimal / 10;
            numeroConvertido += constantes.Dieces[numeroAuxiliar];
            numeroDecimal %= 10;

            //Buscamos los Unitarios
            numeroConvertido += constantes.Unitarios[numeroDecimal];

            return numeroConvertido;
        }
    }
}
